import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  cabecalho: {
    backgroundColor: '#466257',
    padding: 15,
  },
  cabecalhoMenu: {
    backgroundColor: '#e5e5e5',
    padding: 15,
  },
  tituloPagina: {
    paddingBottom: 0,
    marginLeft: -width * 0.17,
    color: '#ffffff',
    fontWeight: 'bold',
  },
  botaoNavegacao: {
    marginBottom: height * 0.001,
    marginRight: width * 0.1,
    marginLeft: width * 0.0005,
  },
  itemMenu: {
    color: '#ffffff',
    marginLeft: width * 0.02,
  },
  subItemMenu: {
    color: '#ffffff',
    marginLeft: width * 0.06,
  },
  subTitulo: {
    fontWeight: 'bold',
    color: '#5b5f5e',
    fontFamily: 'Roboto',
    marginTop: '5%',
  },
  textoCorpo: {
    fontFamily: 'Roboto',
    color: '#5b5f5e',
    marginTop: '5%',
    marginBottom: '5%',
    marginLeft: '2%',
    marginRight: '2%',
    textAlign: 'justify',
  },
  textoInferior: {
    fontFamily: 'Roboto',
    color: '#5b5f5e',
    marginTop: '5%',
    marginBottom: '5%',
    marginLeft: '2%',
    marginRight: '2%',
    textAlign: 'center',
  },
  quadroFinal: {
    backgroundColor: '#466257',
    paddingLeft: '3%',
    paddingRight: '3%',
    marginLeft: width * 0.01,
    marginRight: width * 0.01,
  },
  textoFinal: {
    fontFamily: 'Roboto',
    color: '#ffffff',
    textAlign: 'justify',
    marginBottom: '2%',
  },
  textoLink: {
    textDecorationLine: 'underline',
    color: 'blue',
  },
  iconeMenu: {
    width: width * 0.06,
    height: height * 0.03,
    marginBottom: -height * 0.003,
    marginLeft: width * 0.00009,
  },
  iconeSocial: {
    resizeMode: 'contain',
    width: width * 0.15,
    height: height * 0.065,
    marginBottom: height * 0.003,
    marginLeft: -width * 0.015,
  },
  iconeStartUp: {
    width: width * 0.1,
    height: height * 0.05,
  },
  iconeSRE: {
    width: width * 0.1,
    height: height * 0.05,
  },
  textoBotao: {
    width: width * 0.3,
    textAlign: 'center',
    marginLeft: -width * 0.007,
    color: '#5b5f5e',
  },
  textoBotaoGrande: {
    width: width * 0.3,
    textAlign: 'center',
    marginLeft: -width * 0.007,
    color: '#5b5f5e',
  },
  imagemPrincipal: {
    resizeMode: 'stretch',
    width: width,
    height: height * 0.25,
  },
  imagemCorpoFina: {
    resizeMode: 'stretch',
    width: width,
    height: height * 0.18,
  },
  imagemCorpo: {
    resizeMode: 'center',
    marginTop: '5%',
    alignSelf: 'center',
    width: width * 0.8,
    height: height * 0.6,
  },
  rodape: {
    flexDirection: 'row',
    marginTop: '5%',
    marginBottom: '5%',
  },
  linhaTexto: {
    backgroundColor: '#5b5f5e',
    height: 2,
    flex: 1,
    alignSelf: 'center',
  },
  textoRodape: {
    alignSelf: 'center',
    color: '#5b5f5e',
    paddingHorizontal: 5,
    fontSize: 11,
  },
  botaoInstagram: {
    alignSelf: 'center',
    marginLeft: width * 0.35,
    marginTop: '1%',
    marginBottom: '5%',
    width: width * 0.15,
    height: height * 0.065,
  },
  botaoYouTube: {
    alignSelf: 'center',
    marginLeft: width * 0.04,
    marginTop: '1%',
    marginBottom: '5%',
    width: width * 0.15,
    height: height * 0.065,
  },
  botaoStartUp: {
    alignSelf: 'center',
    marginLeft: width * 0.38,
    marginTop: height * 0.04,
    marginBottom: height * 0.05,
    width: width * 0.05,
    height: height * 0.04,
  },
  botaoSRE: {
    alignSelf: 'center',
    marginLeft: width * 0.15,
    marginTop: height * 0.001,
    marginBottom: height * 0.05,
    width: width * 0.05,
    height: height * 0.04,
  },
  iconeRodape: {
    alignSelf: 'center',
    width: width * 0.05,
    height: height * 0.04,
  },
});
