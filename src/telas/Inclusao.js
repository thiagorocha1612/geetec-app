import React, {Component} from 'react';
import {View, Image, Linking, StatusBar} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import styles from '../estilos/Estilos';

export default class Inclusao extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.cabecalho}>
          <StatusBar backgroundColor="#466257" />
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Image
                style={styles.iconeMenu}
                source={require('../../graficos/icones/iconeMenu.jpg')}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.tituloPagina}>Inclusão Digital</Title>
          </Body>
        </Header>
        <Content>
          <Image
            style={styles.imagemPrincipal}
            source={require('../../graficos/imagens/inclusaoDigital.jpg')}
          />
          <View style={{flex: 1}}>
            <Text style={styles.textoCorpo}>
              Muitas escolas dispõem de laboratórios de informática e acesso à
              internet. Porém, nem sempre contam com profissionais
              especializados em treinamentos de informática para facilitar a
              inclusão digital dos alunos e da comunidade escolar. Desde 2010 ,
              atuamos com o intuito de capacitar alunos e professores de escolas
              públicas capacitando na utilização de computadores e internet.
              Oferecemos treinamentos básicos de informática, de uso de
              computadores, sistemas operacionais e do pacote Libre Office em
              escolas públicas da região do Alto Paraopeba - MG.
            </Text>
          </View>
          <View style={styles.rodape}>
            <View style={styles.linhaTexto} />
            <Text style={styles.textoRodape}>
              Siga-nos em nossas redes sociais
            </Text>
            <View style={styles.linhaTexto} />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button
              transparent
              style={styles.botaoInstagram}
              onPress={() =>
                Linking.openURL(
                  'https://www.google.com/url?q=https%3A%2F%2Fwww.instagram.com%2Fgeetec.ufsj%2F%3Fhl%3Dpt-br&sa=D&sntz=1&usg=AFQjCNHt7jwgAH7aCYfBPs2IbiVetQxEwA',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/socialNetwork.png')}
              />
            </Button>

            <Button
              transparent
              style={styles.botaoYouTube}
              onPress={() =>
                Linking.openURL(
                  'https://www.youtube.com/channel/UCsVtyRbGTMgvIJr7zfcpEDA/',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/youtube.png')}
              />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
