import React, {Component} from 'react';
import {View, Image, Linking, StatusBar} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import styles from '../estilos/Estilos';

export default class Seguranca extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.cabecalho}>
          <StatusBar backgroundColor="#466257" />
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Image
                style={styles.iconeMenu}
                source={require('../../graficos/icones/iconeMenu.jpg')}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.tituloPagina}>Segurança na Internet</Title>
          </Body>
        </Header>
        <Content>
          <Image
            style={styles.imagemPrincipal}
            source={require('../../graficos/imagens/seguranca.jpg')}
          />
          <View style={{flex: 1}}>
            <Text style={styles.textoCorpo}>
              A internet está presente no cotidiano de grande parte da população
              e, provavelmente, para estas pessoas seria muito difícil imaginar
              como seria a vida sem poder usufruir das diversas facilidades e
              oportunidades trazidas por esta tecnologia.
            </Text>
            <Text style={styles.textoCorpo}>
              Aproveitar esses benefícios de forma segura, entretanto, requer
              que alguns cuidados sejam tomados. Para isso, é importante que
              você esteja informado dos riscos aos quais está exposto para que
              possa tomar as medidas preventivas necessárias. Foi preparada uma
              apresentação com informações importantes e vídeos educativos sobre
              como se proteger na internet, cuidando da segurança de sua
              identidade e de seus dados.
            </Text>
          </View>
          <View style={styles.rodape}>
            <View style={styles.linhaTexto} />
            <Text style={styles.textoRodape}>
              Siga-nos em nossas redes sociais
            </Text>
            <View style={styles.linhaTexto} />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button
              transparent
              style={styles.botaoInstagram}
              onPress={() =>
                Linking.openURL(
                  'https://www.google.com/url?q=https%3A%2F%2Fwww.instagram.com%2Fgeetec.ufsj%2F%3Fhl%3Dpt-br&sa=D&sntz=1&usg=AFQjCNHt7jwgAH7aCYfBPs2IbiVetQxEwA',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/socialNetwork.png')}
              />
            </Button>

            <Button
              transparent
              style={styles.botaoYouTube}
              onPress={() =>
                Linking.openURL(
                  'https://www.youtube.com/channel/UCsVtyRbGTMgvIJr7zfcpEDA/',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/youtube.png')}
              />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
