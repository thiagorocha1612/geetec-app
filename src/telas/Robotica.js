import React, {Component} from 'react';
import {View, Image, Linking, StatusBar} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import styles from '../estilos/Estilos';

export default class Robotica extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.cabecalho}>
          <StatusBar backgroundColor="#466257" />
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Image
                style={styles.iconeMenu}
                source={require('../../graficos/icones/iconeMenu.jpg')}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.tituloPagina}>Eletrôncia e Robótica</Title>
          </Body>
        </Header>
        <Content>
          <Image
            style={styles.imagemPrincipal}
            source={require('../../graficos/imagens/robotica.jpg')}
          />
          <View style={{flex: 1}}>
            <Text style={styles.textoCorpo}>
              Com o avanço das tecnologias de automação, a robótica se tornou
              uma área muito interessante e desperta a curiosidade em várias
              pessoas. Pensando neste interesse pela robótica, e buscando formas
              de ensino que não gerem custo para as escolas, o GEETEC oferece
              treinamentos de eletrônica e robótica utilizando a ferramenta de
              simulação de circuitos Thinkercad, que possibilitam estes
              treinamentos sem a necessidade de compra de material por parte das
              escolas.
            </Text>
            <Text style={styles.textoCorpo}>
              Esta ferramenta permite construir circuitos ou utilizar
              plataformas mais avançadas, como o Arduíno, realizando sua
              programação através de blocos de código e simulando seu
              funcionamento de forma totalmente virtual.
            </Text>
            <Text style={styles.textoCorpo}>
              Estas atividades trazem novas ideias de conteúdos que podem ser
              trabalhados nas escolas, além de proporcionar uma evolução no
              raciocínio lógico dos alunos, mostra que o ensino de robótica não
              precisa ser algo custoso, que foge da realidade das escolas
              públicas.
            </Text>

            <Image
              style={{marginBottom: '5%', alignSelf: 'center'}}
              source={require('../../graficos/imagens/Arduino.png')}
            />
          </View>
          <View style={styles.rodape}>
            <View style={styles.linhaTexto} />
            <Text style={styles.textoRodape}>
              Siga-nos em nossas redes sociais
            </Text>
            <View style={styles.linhaTexto} />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button
              transparent
              style={styles.botaoInstagram}
              onPress={() =>
                Linking.openURL(
                  'https://www.google.com/url?q=https%3A%2F%2Fwww.instagram.com%2Fgeetec.ufsj%2F%3Fhl%3Dpt-br&sa=D&sntz=1&usg=AFQjCNHt7jwgAH7aCYfBPs2IbiVetQxEwA',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/socialNetwork.png')}
              />
            </Button>

            <Button
              transparent
              style={styles.botaoYouTube}
              onPress={() =>
                Linking.openURL(
                  'https://www.youtube.com/channel/UCsVtyRbGTMgvIJr7zfcpEDA/',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/youtube.png')}
              />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
