import React, {Component} from 'react';
import {View, Image, Linking, StatusBar} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import styles from '../estilos/Estilos';

export default class Atuacao extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.cabecalho}>
          <StatusBar backgroundColor="#466257" />
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Image
                style={styles.iconeMenu}
                source={require('../../graficos/icones/iconeMenu.jpg')}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.tituloPagina}>Áreas de Atuação</Title>
          </Body>
        </Header>
        <Content>
          <Image
            style={styles.imagemPrincipal}
            source={require('../../graficos/imagens/AreasDeAtuação.jpg')}
          />
          <View style={{flex: 1}}>
            <Text style={styles.textoCorpo}>
              Nós já atuamos em diversos setores relacionados a tecnologias
              computacionais. Inicialmente melhoramos a Infraestrutura de rede e
              dos laboratórios de informática de escolas públicas municipais,
              seguimos com a Inclusão Digital e, atualmente, trabalhamos também
              com o Ensino de linguagem de programação de computadores,
              introdução a Robótica e construção de jogos relacionado com o
              ensino. Também oferecemos palestras relacionadas com informática e
              segurança da informação e trabalhos que desenvolvam o
              Empreendedorismo e a Inovação.
            </Text>
          </View>
          <View style={styles.quadroFinal}>
            <Text style={styles.textoFinal}>
              Rodovia MG 443, KM 7, Ouro Branco – MG,
            </Text>
            <Text style={styles.textoFinal}>36420-000</Text>
            <Text style={styles.textoFinal}>geetec-cap@ufsj.edu.br</Text>
          </View>
          <View style={styles.rodape}>
            <View style={styles.linhaTexto} />
            <Text style={styles.textoRodape}>
              Siga-nos em nossas redes sociais
            </Text>
            <View style={styles.linhaTexto} />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button
              transparent
              style={styles.botaoInstagram}
              onPress={() =>
                Linking.openURL(
                  'https://www.google.com/url?q=https%3A%2F%2Fwww.instagram.com%2Fgeetec.ufsj%2F%3Fhl%3Dpt-br&sa=D&sntz=1&usg=AFQjCNHt7jwgAH7aCYfBPs2IbiVetQxEwA',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/socialNetwork.png')}
              />
            </Button>

            <Button
              transparent
              style={styles.botaoYouTube}
              onPress={() =>
                Linking.openURL(
                  'https://www.youtube.com/channel/UCsVtyRbGTMgvIJr7zfcpEDA/',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/youtube.png')}
              />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
