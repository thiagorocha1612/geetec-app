import React from 'react';
import {View, Text, Button} from 'react-native';
import TelaLateral from './TelaLateral';
import {createAppContainer, withNavigation} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import TelaInicial from '../telas/HomeScreen';

const MainNavigator = createStackNavigator(
  {
    TelaInicial: {screen: TelaInicial},
    TelaLateral: {screen: TelaLateral},
  },
  {
    initialRouteName: 'TelaLateral',
    headerMode: 'none',
    swipeEnabled: false,
  },
);

const MainRoute = createAppContainer(MainNavigator);
export default MainRoute;
