import React, {Component} from 'react';
import {View, Image, Linking, StatusBar} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import styles from '../estilos/Estilos';

export default class SobreNos extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.cabecalho}>
          <StatusBar backgroundColor="#466257" />
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Image
                style={styles.iconeMenu}
                source={require('../../graficos/icones/iconeMenu.jpg')}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.tituloPagina}>Sobre Nós</Title>
          </Body>
        </Header>
        <Content>
          <Image
            style={styles.imagemPrincipal}
            source={require('../../graficos/imagens/SobreNos.jpg')}
          />
          <View style={{flex: 1}}>
            <Text style={styles.textoCorpo}>
              O Grupo de Extensão no Ensino de Tecnologias Computacionais
              (GEETEC) é uma iniciativa que conta com a colaboração de
              professores e alunos da UFSJ - Campus Alto Paraopeba, localizado
              em Ouro Branco - MG. Este grupo visa promover a Inclusão digital
              tendo como ênfases a programação de computadores, a introdução à
              robótica, informática e tecnologia da informação incentivando a
              inovação e o empreendedorismo. Mantemos parcerias com o grupo de
              extensão Startup CAP e com as Secretarias de Educação Estadual e
              Municipais.
            </Text>

            <Text style={styles.textoInferior}>
              Saiba mais sobre nossa trajetória em nosso site,{' '}
              <Text
                style={styles.textoLink}
                onPress={() =>
                  Linking.openURL('https://sites.google.com/view/geetec/')
                }>
                clicando aqui
              </Text>
              .
            </Text>
            <Text style={styles.textoInferior}>
              Em caso de dúvidas, entre em contato com a gente.
            </Text>
          </View>
          <View style={styles.quadroFinal}>
            <Text style={styles.textoFinal}>
              Rodovia MG 443, KM 7, Ouro Branco – MG,
            </Text>
            <Text style={styles.textoFinal}>36420-000</Text>
            <Text style={styles.textoFinal}>geetec-cap@ufsj.edu.br</Text>
          </View>
          <View style={styles.rodape}>
            <View style={styles.linhaTexto} />
            <Text style={styles.textoRodape}>
              Siga-nos em nossas redes sociais
            </Text>
            <View style={styles.linhaTexto} />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button
              transparent
              style={styles.botaoInstagram}
              onPress={() =>
                Linking.openURL(
                  'https://www.google.com/url?q=https%3A%2F%2Fwww.instagram.com%2Fgeetec.ufsj%2F%3Fhl%3Dpt-br&sa=D&sntz=1&usg=AFQjCNHt7jwgAH7aCYfBPs2IbiVetQxEwA',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/socialNetwork.png')}
              />
            </Button>

            <Button
              transparent
              style={styles.botaoYouTube}
              onPress={() =>
                Linking.openURL(
                  'https://www.youtube.com/channel/UCsVtyRbGTMgvIJr7zfcpEDA/',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/youtube.png')}
              />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
