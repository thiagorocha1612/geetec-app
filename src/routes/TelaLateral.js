import React from 'react';
import {View, Text, Button, Dimensions} from 'react-native';
import {createAppContainer, withNavigation} from 'react-navigation';
import 'react-native-gesture-handler';
import 'react-native-reanimated';
import {createDrawerNavigator} from 'react-navigation-drawer';
import BarraLateral from '../barraLateral/Menu';
import TelaInicial from '../telas/HomeScreen';
import SobreNos from '../telas/SobreNos';
import Robotica from '../telas/Robotica';
import Inclusao from '../telas/Inclusao';
import Parcerias from '../telas/Parcerias';
import Programacao from '../telas/Programacao';
import Seguranca from '../telas/Seguranca';
import Atuacao from '../telas/Atuacao';

const WIDTH = Dimensions.get('window').width;

const TelaLateral = createDrawerNavigator(
  {
    TelaInicial: {screen: TelaInicial},
    Atuacao: {screen: Atuacao},
    Inclusao: {screen: Inclusao},
    Parcerias: {screen: Parcerias},
    Programacao: {screen: Programacao},
    Robotica: {screen: Robotica},
    Seguranca: {screen: Seguranca},
    SobreNos: {screen: SobreNos},
  },
  {
    initialRouteName: 'TelaInicial',
    drawerWidth: WIDTH * 0.8,
    drawerPosition: 'left',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
    contentComponent: props => <BarraLateral {...props} />,
    drawerOpenRoute: 'LeftSideMenu',
    drawerCloseRoute: 'LeftSideMenuClose',
    drawerToggleRoute: 'LeftSideMenuToggle',
  },
);
export default TelaLateral;
