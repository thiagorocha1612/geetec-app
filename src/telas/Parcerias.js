import React, {Component} from 'react';
import {View, Image, Linking, StatusBar} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import styles from '../estilos/Estilos';

export default class Parcerias extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.cabecalho}>
          <StatusBar backgroundColor="#466257" />
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Image
                style={styles.iconeMenu}
                source={require('../../graficos/icones/iconeMenu.jpg')}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.tituloPagina}>Parcerias</Title>
          </Body>
        </Header>
        <Content>
          <Image
            style={styles.imagemPrincipal}
            source={require('../../graficos/imagens/SobreNos.jpg')}
          />
          <View style={{flex: 1}}>
            <Text style={styles.textoCorpo}>
              O grupo GEETEC está sempre em busca de desafios e pretende
              fomentar junto com suas atividades o empreendedorismo e a
              inovação. Diante disso, em 2019, foi feita uma parceria com o
              projeto de extensão{' '}
              <Text
                style={styles.textoLink}
                onPress={() =>
                  Linking.openURL(
                    'https://www.instagram.com/startupcapufsj/?hl=pt-br',
                  )
                }>
                Startup CAP
              </Text>
              , ambos da Universidade Federal de São João Del-Rei.
            </Text>
            <Text style={styles.textoCorpo}>
              Ter uma visão mais assertiva das ações e atividades que devem ser
              oferecidas nas escolas também é de extrema importância. Assim,
              contamos também com o apoio da{' '}
              <Text
                style={styles.textoLink}
                onPress={() =>
                  Linking.openURL('https://srelafaiete.educacao.mg.gov.br/')
                }>
                Superintendência Regional de Ensino - SRE
              </Text>
              , o que possibilita entender as reais necessidades da comunidade,
              focando em áreas de maior carência.
            </Text>
            <Text style={styles.textoCorpo}>
              Com essa parceria, trabalhamos com atividades integradas as
              disciplinas vistas em sala de aula e, como resultado, alcançamos
              melhores desempenhos das notas e percebemos o aumento da
              frequência dos alunos nas aulas.
            </Text>
          </View>
          <View style={styles.rodape}>
            <View style={styles.linhaTexto} />
            <Text style={styles.textoRodape}>
              Siga-nos em nossas redes sociais
            </Text>
            <View style={styles.linhaTexto} />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button
              transparent
              style={styles.botaoInstagram}
              onPress={() =>
                Linking.openURL(
                  'https://www.google.com/url?q=https%3A%2F%2Fwww.instagram.com%2Fgeetec.ufsj%2F%3Fhl%3Dpt-br&sa=D&sntz=1&usg=AFQjCNHt7jwgAH7aCYfBPs2IbiVetQxEwA',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/socialNetwork.png')}
              />
            </Button>

            <Button
              transparent
              style={styles.botaoYouTube}
              onPress={() =>
                Linking.openURL(
                  'https://www.youtube.com/channel/UCsVtyRbGTMgvIJr7zfcpEDA/',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/youtube.png')}
              />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
