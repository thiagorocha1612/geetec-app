import React, {Component} from 'react';
import {View, Image, Linking, StatusBar} from 'react-native';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import 'react-navigation';
import styles from '../estilos/Estilos';
import {DrawerActions} from 'react-navigation-drawer';
import {BarraLateral} from '../barraLateral/Menu';
import SobreNos from './SobreNos';

export default class TelaInicial extends Component {
  render() {
    return (
      <Container>
        <View>
          <Header style={styles.cabecalho}>
            <StatusBar backgroundColor="#466257" />
            <Left>
              <Button
                transparent
                style={styles.botaoNavegacao}
                onPress={() =>
                  this.props.navigation.dispatch(DrawerActions.openDrawer())
                }>
                <Image
                  style={styles.iconeMenu}
                  source={require('../../graficos/icones/iconeMenu.jpg')}
                />
              </Button>
            </Left>
            <Body>
              <Title style={styles.tituloPagina}>GEETEC</Title>
            </Body>
          </Header>
        </View>
        <Content>
          <Image
            style={styles.imagemPrincipal}
            source={require('../../graficos/imagens/inicio.jpg')}
          />
          <Image
            style={styles.imagemCorpo}
            source={require('../../graficos/imagens/infoGraficoHD.jpg')}
          />
          <View style={styles.rodape}>
            <View style={styles.linhaTexto} />
            <Text style={styles.textoRodape}>
              Siga-nos em nossas redes sociais
            </Text>
            <View style={styles.linhaTexto} />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button
              transparent
              style={styles.botaoInstagram}
              onPress={() =>
                Linking.openURL(
                  'https://www.google.com/url?q=https%3A%2F%2Fwww.instagram.com%2Fgeetec.ufsj%2F%3Fhl%3Dpt-br&sa=D&sntz=1&usg=AFQjCNHt7jwgAH7aCYfBPs2IbiVetQxEwA',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/socialNetwork.png')}
              />
            </Button>

            <Button
              transparent
              style={styles.botaoYouTube}
              onPress={() =>
                Linking.openURL(
                  'https://www.youtube.com/channel/UCsVtyRbGTMgvIJr7zfcpEDA/',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/youtube.png')}
              />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
