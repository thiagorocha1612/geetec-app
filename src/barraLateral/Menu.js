import React, {Component} from 'react';
import {
  Content,
  List,
  Header,
  Body,
  Title,
  ListItem,
  Container,
  Left,
  Right,
  Icon,
  Text,
  Badge,
} from 'native-base';
import styles from '../estilos/Estilos';
import {ScrollView} from 'react-native-gesture-handler';
import {NavigationActions, withNavigation} from 'react-navigation';

export default class BarraLateral extends React.Component {
  constructor(props) {
    super(props);
  }

  navigateToScreen = route => () => {
    const navigate = NavigationActions.navigate({
      routeName: route,
    });

    this.props.navigation.dispatch(navigate);
  };
  render() {
    return (
      <ScrollView>
        <Container>
          <Content style={{backgroundColor: '#466257'}}>
            <List style={{backgroundColor: '#466257', marginLeft: -20}}>
              <ListItem
                onPress={() => this.props.navigation.navigate('TelaInicial')}
                selected>
                <Left>
                  <Text style={styles.itemMenu}>Início</Text>
                </Left>
                <Right />
              </ListItem>
              <ListItem
                onPress={() => this.props.navigation.navigate('SobreNos')}>
                <Left>
                  <Text style={styles.itemMenu}>Sobre Nós</Text>
                </Left>
                <Right />
              </ListItem>
              <ListItem
                onPress={() => this.props.navigation.navigate('Atuacao')}>
                <Left>
                  <Text style={styles.itemMenu}>Áreas de atuação</Text>
                </Left>
                <Right />
              </ListItem>
              <ListItem
                onPress={() => this.props.navigation.navigate('Inclusao')}>
                <Left>
                  <Text style={styles.subItemMenu}>Inclusão Digital</Text>
                </Left>
                <Right />
              </ListItem>
              <ListItem
                onPress={() => this.props.navigation.navigate('Robotica')}>
                <Left>
                  <Text style={styles.subItemMenu}>Eletrônica e Robótica</Text>
                </Left>
                <Right />
              </ListItem>
              <ListItem
                onPress={() => this.props.navigation.navigate('Programacao')}>
                <Left>
                  <Text style={styles.subItemMenu}>Ling. de Programação</Text>
                </Left>
                <Right />
              </ListItem>
              <ListItem
                onPress={() => this.props.navigation.navigate('Seguranca')}>
                <Left>
                  <Text style={styles.subItemMenu}>Segurança na Internet</Text>
                </Left>
                <Right />
              </ListItem>
              <ListItem
                onPress={() => this.props.navigation.navigate('Parcerias')}>
                <Left>
                  <Text style={styles.itemMenu}>Parcerias</Text>
                </Left>
                <Right />
              </ListItem>
            </List>
          </Content>
        </Container>
      </ScrollView>
    );
  }
}
