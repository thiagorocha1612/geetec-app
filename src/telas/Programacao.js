import React, {Component} from 'react';
import {View, Image, Linking, StatusBar} from 'react-native';
import Hyperlink from 'react-native-hyperlink';
import {
  Container,
  Header,
  Content,
  Button,
  Text,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import styles from '../estilos/Estilos';

export default class Programacao extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.cabecalho}>
          <StatusBar backgroundColor="#466257" />
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Image
                style={styles.iconeMenu}
                source={require('../../graficos/icones/iconeMenu.jpg')}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.tituloPagina}>Ling. de Programação</Title>
          </Body>
        </Header>
        <Content>
          <Image
            style={styles.imagemPrincipal}
            source={require('../../graficos/imagens/lingDeProgramacao.jpg')}
          />
          <View style={{flex: 1}}>
            <Text style={styles.subTitulo}>
              {' '}
              - Programação para Crianças e Adolescentes:
            </Text>
            <Text style={styles.textoCorpo}>
              A Programação para Crianças e Adolescentes tem como objetivo levar
              aos alunos das escolas públicas conhecimentos básicos de
              informática e um primeiro contato com a programação de
              computadores.
            </Text>
            <Text style={styles.textoCorpo}>
              O ensino da programação de computadores será desenvolvido através
              de programas como o Scratch, utilizando uma abordagem de criação
              de desafios relacionados às disciplinas tradicionais como
              matemática, português, química, entre outras.
            </Text>
            <Text style={styles.textoCorpo}>
              Além do Scratch, a plataforma Code.org, que permite a utilização
              de jogos eletrônicos populares no ensino de programação também é
              utilizada como forma de aumentar a atratividade dos treinamentos
              de programação e mostrando que aprender também pode ser divertido.
            </Text>

            <Image
              style={styles.imagemPrincipal}
              source={require('../../graficos/imagens/printCode.png')}
            />
            <Text style={styles.subTitulo}>
              {' '}
              - Preparação para a Olimpíada Brasileira de Informática - OBI:
            </Text>
            <Text style={styles.textoCorpo}>
              A Olimpíada Brasileira de Informática (OBI) é uma competição, que
              tem como intuito despertar o interesse dos alunos a conhecerem a
              ciência da computação. Atualmente ela é organizada em duas
              modalidades: iniciação e programação.
            </Text>
            <Text style={styles.textoCorpo}>
              Na modalidade iniciação, os alunos resolvem problemas lógicos e de
              raciocínio computacional no papel, sem uso do computador. O
              objetivo desta modalidade é despertar o gosto pela programação e
              identificar novos talentos para a área. Podem se inscrever alunos
              do 4º ao 9º ano do ensino fundamental.
            </Text>
            <Text style={styles.textoCorpo}>
              A outra modalidade exige como requisito que o aluno participante
              tenha um conhecimento prévio de programação, já que a prova contém
              questões de estruturas de dados, algoritmos e técnicas de
              programação. Nesta, podem se inscrever alunos a partir do 9º ano
              do ensino fundamental até alunos que estejam cursando, pela
              primeira vez, o primeiro ano de um curso de graduação.
            </Text>
            <Text style={styles.textoCorpo}>
              Os alunos que apresentarem o melhor desempenho recebem premiações
              e os melhores colocados da modalidade programação nível 2, que são
              alunos que estão até o 3º ano do ensino médio, passarão por uma
              seleção final que irá escolher 4 alunos para participarem da
              equipe brasileira na Olimpíada Internacional de Informática.
            </Text>
            <Text style={styles.textoCorpo}>
              Nos nossos treinamentos, ensinamos a lógica de programação fazendo
              o uso do Portugol Studio. Após esse conhecimento estar
              consolidado, migramos para a linguagem C++ e passamos a
              explorá-la. Preparamos os alunos com exercícios no estilo da OBI
              ou de outras maratonas semelhantes através da plataforma URI, que
              além de disponibilizar os exercícios, julga os códigos fontes
              submetidos criando uma ordenação de alunos.
            </Text>
            <Image
              style={styles.imagemCorpoFina}
              source={require('../../graficos/imagens/Obi.png')}
            />
          </View>
          <View style={styles.rodape}>
            <View style={styles.linhaTexto} />
            <Text style={styles.textoRodape}>
              Siga-nos em nossas redes sociais
            </Text>
            <View style={styles.linhaTexto} />
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button
              transparent
              style={styles.botaoInstagram}
              onPress={() =>
                Linking.openURL(
                  'https://www.google.com/url?q=https%3A%2F%2Fwww.instagram.com%2Fgeetec.ufsj%2F%3Fhl%3Dpt-br&sa=D&sntz=1&usg=AFQjCNHt7jwgAH7aCYfBPs2IbiVetQxEwA',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/socialNetwork.png')}
              />
            </Button>

            <Button
              transparent
              style={styles.botaoYouTube}
              onPress={() =>
                Linking.openURL(
                  'https://www.youtube.com/channel/UCsVtyRbGTMgvIJr7zfcpEDA/',
                )
              }>
              <Image
                style={styles.iconeSocial}
                source={require('../../graficos/icones/youtube.png')}
              />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
